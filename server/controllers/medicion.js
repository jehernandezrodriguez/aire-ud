'use strict'
// Cargamos los modelos para usarlos posteriormente
var Medicion = require('../models/medicion');
const moment = require('moment')
// Conseguir datos de un usuario
function getMedicionById(req, res){
    var userId = req.params.id;
//buscar un documento por un  id
    Medicion.findById(userId, (err, medicion) =>
    {

        if(err)return res.status(500).send({message: 'Error en la petición'});
        if(!medicion) return res.status(404).send({message: 'EL usuario no existe'});

      followThisUser(req.user.sub, userId).then((value) => {
            medicion.password = undefined;
            return res.status(200).send({
                medicion,
                following: value.following,
                followed: value.followed
            });
        });

    });
}

function getAll(req, res){
  console.log("consultado todas las medicionesS");
  Medicion.find({}, (err, mediciones)=>{
        if(err) return res.status(500).send({message:`Ha ocurrido un Error ${err}`})
        if(!mediciones) return res.status(204).send({message:'No se encontraron registros'})
        console.log(mediciones);
        return res.status(200).send(mediciones)
  })

}

function getAllCompo(req, res){


  Medicion.aggregate([
    {$group: { "_id": "$componente", "number": {"$sum": 1}}}

]).exec((err, mediciones) => {
  if(err) return res.status(500).send({message:`Ha ocurrido un Error ${err}`})

  return res.status(200).send(mediciones)
})

}

function delet(req, res){
    Medicion.findByIdAndRemove( req.params.id , (err, success)=>
    {
        if(err) return res.status(500).send({msg:'Ocurrio un error al eliminar el miembro del equipo', err:err})
        if (success)
          res.status(200).send({msg:'La prueba se ha borrado con exito '})
        else {
            res.status(200).send({msg:'Ocurrio un error al borrar la prueba.'})
        }
    })

}

function getTest(req, res){

  Medicion.find({}, (err, mediciones)=>{
        if(err) return res.status(500).send({message:`Ha ocurrido un Error ${err}`})
        if(!mediciones) return res.status(204).send({message:'No se encontraron registros'})
        console.log(mediciones);
        var html = "<center> Aqui se pueden consultar las mediciones de los dispositivos</center> ";
        html += "<center> Total de mediciones: "+mediciones.length+"</center>";

        for (var i = 0; i < mediciones.length; i++) {
          html+="<hr> <br>"
          html+= `ID medición: ${mediciones[i]._id} <br>`
          html+= `fecha: ${mediciones[i].date} <br>`
          html+= `medicion: ${mediciones[i].medicion} <br>`
          html+= `lat: ${mediciones[i].lat} <br>`
          html+= `lng: ${mediciones[i].lng} <br>`
          html+= `componente: ${mediciones[i].componente} <br>`
          html+= `<a href="/api/mediciones/delete/${mediciones[i]._id}" target="_blank">¿Eliminar prueba?</a> <br>`

        }
        return res.status(200).send(html)
  })

}

function nuevaMedicion (data){
    const medicion = new Medicion(data)
    medicion.save( (err) => {
        if(err) console.log(err);
        console.log("Medición guardada");
    })
}

module.exports = {
    getMedicionById,
    getAll,getTest,
    nuevaMedicion,
    delet,getAllCompo
}
