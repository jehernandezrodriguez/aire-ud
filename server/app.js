'use strict'
// Cargamos los módulos de express y body-parser
var express = require('express');
var bodyParser = require('body-parser');
const basicAuth = require('express-basic-auth')
const api = require('./routes/routes');
const cors = require('cors')

// Llamamos a express para poder crear el servidor
var app = express();
// Importamos las rutas
//cargar middlewares
//un metodo que se ejecuta antes que llegue a un controlador
//Configuramos bodyParser para que convierta el body de nuestras peticiones a JSON
const corsOptions = {
  origin:'http://localhost:3000',
  credentials:true
}

app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

app.use(cors(corsOptions))

app.use(function(req, res, next) {
    res.setHeader("Access-Control-Allow-Methods", "POST, GET");
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method, Authorization");
    res.header("Access-Control-Allow-Credentials", true);
    next();
  });


// Cargamos las rutas
app.use('/api', api);
// exportamos este módulo para poder usar la variable app fuera de este archivo
app.use('/', express.static(__dirname + '/public'));

app.use(express.static('public'))
app.use(express.static('public/static'))

module.exports = app;
