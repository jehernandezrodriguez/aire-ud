'use strict'
// Cargamos el módulo de express para poder crear rutas
var express = require('express');
// Cargamos el controlador
var MedicionController = require('../controllers/medicion');
var ComponenteController = require('../controllers/componente');
// Llamamos al router
var api = express.Router();
var md_auth = require('../middlewares/authenticated');

const basicAuth = require('express-basic-auth');

// Creamos una ruta para los métodos que tenemos en nuestros controladores
api.get('/medicion/:id',basicAuth({ users: { 'aire': 'Imb4T3st4pp_334' },}), MedicionController.getMedicionById);
api.get('/mediciones',basicAuth({ users: { 'aire': 'Imb4T3st4pp_334' },}), MedicionController.getAll);


api.get('/medicionesByComponente',basicAuth({ users: { 'aire': 'Imb4T3st4pp_334' },}), MedicionController.getAllCompo);


api.get('/mediciones-test', MedicionController.getTest);
api.get('/mediciones/delete/:id', MedicionController.delet);

api.get('/componentes',basicAuth({ users: { 'aire': 'Imb4T3st4pp_334' },}), ComponenteController.getAll);
api.post('/componte-gestion',basicAuth({ users: { 'aire': 'Imb4T3st4pp_334' },}), ComponenteController.gestion);
// Exportamos la configuración
module.exports = api;
