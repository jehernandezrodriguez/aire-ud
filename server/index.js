'use strict'

var mongoose = require('mongoose');
const app = require('./app');
const config = require('./config')

  // Usamos el método connect para conectarnos a nuestra base de datos
  mongoose.connect(config.db,  { useMongoClient: true})
      .then(() => {
          // Cuando se realiza la conexión, lanzamos este mensaje por consola
          console.log("La conexión a la base de datos se ha realizado correctamente")

          // CREAR EL SERVIDOR WEB CON NODEJS
          app.listen(config.port, () => {
              console.log(`servidor corriendo en http://localhost:${config.port}`);
              var mqtt = require('./mqtt')
          });
      })
      // Si no se conecta correctamente escupimos el error
      .catch(err => console.log(err));
