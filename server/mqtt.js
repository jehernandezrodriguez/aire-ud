
var client = require('./mqtt_connect')
const moment = require('moment')
//conexión a servidor mqtt
var MedicionController = require('./controllers/medicion');
var ComponenteController = require('./controllers/componente');

client.on('connect', function ()
{
  console.log('conectado ...');
  client.subscribe('aire', function (err) {
    if (!err) {
      client.publish('aire', 'Hello mqtt')
    }
  })

  client.subscribe('aire/componente', function (err) {
    if (!err) {
      client.publish('aire/componente', 'server')
    }
  })
})

client.on('message', function (topic, message) {
  // message is Buffer
  topic = topic.toString().trim()

  message = message.toString().trim()
  console.log("Nuevo mensaje en topic",topic);
  console.log("mensaje ", message);

  if (message && topic.trim() == "aire")
  {
    console.log("entro a aire");
      if (isJSON(message))
      {

          dataMe = JSON.parse(message)

          if (isJSON(dataMe))
            dataMe = JSON.parse(dataMe)

          console.log(dataMe, "mensaje");
          console.log(typeof dataMe);
          console.log(dataMe.medicion);
          var data = {
                  medicion: ( dataMe.hasOwnProperty('medicion') ) ? dataMe.medicion : 0.0 ,
                   date: new Date(),
                   lat: ( dataMe.hasOwnProperty('lat') ) ? dataMe.lat : 0.0,
                   lng: ( dataMe.hasOwnProperty('lng') ) ? dataMe.lng : 0.0,
                   componente: (dataMe.hasOwnProperty('id')) ? dataMe.id : "server"
                 }
          console.log("nueva medición",data);
          MedicionController.nuevaMedicion(data);
      }
  }

  if (message && topic.trim() =="aire/componente"){
    console.log("entro a aire/componente");
    ComponenteController.nuevo({componente:message});
  }

})

function isJSON(str) {
    try {
        return (JSON.parse(str) && !!str);
    } catch (e) {
        return false;
    }
}
