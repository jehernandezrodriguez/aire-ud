module.exports = {
    port: process.env.PORT || 3001,
    origin: process.env.ORIGIN || 'http://localhost:4200',
    db: process.env.MONGODB || 'mongodb://userapp:vxYz8f4iCMmVIsbc@cluster0-shard-00-00-2rf9h.mongodb.net:27017/aire?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority',
    SECRET_TOKEN: 'mysecret'
}
