import React, { Component } from 'react';
import {Bar} from 'react-chartjs-2';

export default class BarChartComponent extends Component
{
   constructor(props) {
      super(props);
      this.state = {
        Data: {}
      }
    }

      componentDidMount() {

        fetch('https://aire-app.herokuapp.com/api/medicionesByComponente',{
          headers: new Headers({
            'Authorization': 'Basic '+btoa('aire:Imb4T3st4pp_334'),
            'Content-Type': 'application/json'
            }),
          })
          .then(response => response.json())
          .then(res => {
            const mediciones = res;
            let nombre_dispositivo = [];
            let cantidad_mediciones = [];

            mediciones.forEach(element => {
              nombre_dispositivo.push(element._id);
              cantidad_mediciones.push(element.number);
            });
            this.setState({
              Data: {
                labels: nombre_dispositivo,
                datasets:[
                   {
                      label:'Dispositivos vs Cantidad de mediciones',
                      data: cantidad_mediciones ,
                      backgroundColor:[
                       'rgba(255,105,145,0.6)',
                       'rgba(155,100,210,0.6)',
                       'rgba(90,178,255,0.6)',
                       'rgba(240,134,67,0.6)',
                       'rgba(120,120,120,0.6)',
                       'rgba(250,55,197,0.6)'
                    ]
                   }
                ]
             }
             });
          })
      }
 render()
   {
      return(
        <div>
          <Bar
            data = {this.state.Data}
            options = {{ maintainAspectRatio: false }} />
        </div>
      )
   }
}
