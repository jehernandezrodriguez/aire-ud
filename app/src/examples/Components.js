import React, { Component, Fragment } from 'react';
import isEmpty from 'lodash.isempty';


class Components extends Component {
  constructor(props)
  {
    super(props);

    this.state = {
      components: [],
    };
  }

  componentDidMount() {
    fetch('https://aire-app.herokuapp.com/api/componentes',{
      headers: new Headers({
        'Authorization': 'Basic '+btoa('aire:Imb4T3st4pp_334'),
        'Content-Type': 'application/json'
        }),
      })
      .then(response => response.json())
      .then(data => this.setState({ components: data }));
  }

  render() {
    const { components } = this.state;
    return (
      <Fragment>
        <div class="row">

          <div class="col-12 col-sm-8 offset-sm-2">

          <center><h1>Dispositivos electrónicos </h1></center>
          {components.map((component) => (
            <div class="card">
              <div class="card-body">
                <h5 class="card-title">Componente - {component.componente}</h5>
                <h6 class="card-subtitle mb-2 text-muted"> Total de mediciones: {component.count} </h6> <a>Apagar </a> <a>Encender <i class="fas fa-power-off"></i></a>
              </div>
            </div>
          ))}

          </div>
        </div>

      </Fragment>
    );
  }
}

export default Components;
