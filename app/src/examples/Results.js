import React, { Component, Fragment } from 'react';
import { Chart } from 'react-charts'

import BarChartComponent from '../components/BarChartComponent';
import LineChartComponent from '../components/LineChartComponent';

//import PieChartComponent from '../components/PieChartComponent';

class Results extends Component {

  constructor(props)
  {
    super(props);
    this.state = {
      mediciones: [],
    };
  }

  componentDidMount() {
    fetch('https://aire-app.herokuapp.com/api/mediciones',{
      headers: new Headers({
        'Authorization': 'Basic '+btoa('aire:Imb4T3st4pp_334'),
        'Content-Type': 'application/json'
        }),
      })
      .then(response => response.json())

      .then(data => this.setState({ mediciones: data }));
  }

  render() {
    const { mediciones } = this.state;
    return (
      <Fragment>
        <div class="row">

          <div class="col-12 col-sm-6">

          <center><h1>Listado de mediciones  </h1></center>
          {mediciones.map((medicion) => (
            <div class="card">
              <div class="card-body">
                <h5 class="card-title">Medición - {medicion._id}</h5>
                <h6 class="card-subtitle mb-2 text-muted"> Rango: {medicion.medicion} <a target="_blank" href={`https://www.google.com/maps/search/?api=1&query=${medicion.lat},${medicion.lng} `} > Locación </a></h6>

              </div>
            </div>
          ))}

          </div>

          <div class="col-12 col-sm-6">

          <center><h1> Graficas  </h1></center>


          <div className="App">
              <h4 class="my-3"> Mediciones </h4>
              <LineChartComponent/>
              <h4 class="my-3"> Cantidad de mediciones por Dispositivo</h4>
              <BarChartComponent/>
           </div>

          </div>




        </div>
      </Fragment>
    );
  }
}

export default Results;
