import React, { Component, Fragment } from 'react';
import isEmpty from 'lodash.isempty';


class Report extends Component {
  constructor(props)
  {
    super(props);

    this.state = {
      mediciones: [],
    };
  }

  componentDidMount() {
    fetch('https://aire-app.herokuapp.com/api/mediciones',{
      headers: new Headers({
        'Authorization': 'Basic '+btoa('aire:Imb4T3st4pp_334'),
        'Content-Type': 'application/json'
        }),
      })
      .then(response => response.json())
      .then(data => this.setState({ mediciones: data }));
  }

  render() {
    const { mediciones } = this.state;
    return (
      <Fragment>
        <div class="row">
        
          <div class="col-12 col-sm-6">

          <center><h1>Listado de mediciones  </h1></center>
          {mediciones.map((medicion) => (
            <div class="card">
              <div class="card-body">
                <h5 class="card-title">Medición - {medicion._id}</h5>
                <h6 class="card-subtitle mb-2 text-muted"> Rango: {medicion.medicion} <a target="_blank" href={`https://www.google.com/maps/search/?api=1&query=${medicion.lat},${medicion.lng} `} > Locación </a></h6>

              </div>
            </div>
          ))}

          </div>



        </div>

      </Fragment>
    );
  }
}

export default Report;
