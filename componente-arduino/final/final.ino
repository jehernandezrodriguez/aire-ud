#include <ArduinoJson.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

// datos de wifi
const char* ssid = "........";
const char* password = "........";

const char* mqttServer = "soldier.cloudmqtt.com";
const int mqttPort = 12236;
const char* mqttUser = "yrjxpumk";
const char* mqttPassword = "vOpEMYFHrL2p";


 char* id = "disp_1_"; //id del dispo
//locacion del dispositivo
char* lat = "4.630841";
const char* lng = "-74.066312";

bool enviarDatos = true;

StaticJsonBuffer<280> jsonBuffer;
JsonObject& datos = jsonBuffer.createObject();

WiFiClient espClient;
PubSubClient client(espClient);
unsigned long lastMsg = 0;

/************************Hardware Related Macros************************************/
const int calibrationLed = 13;                      //when the calibration start , LED pin 13 will light up , off when finish calibrating
const int MQ_PIN = A0;                                //define which analog input channel you are going to use
int RL_VALUE = 1;                                     //define the load resistance on the board, in kilo ohms
float RO_CLEAN_AIR_FACTOR =2.1;                     //RO_CLEAR_AIR_FACTOR=(Sensor resistance in clean air)/RO, (Rs/R0)
                                                    //which is derived from the chart in datasheet

/***********************Software Related Macros************************************/
int CALIBARAION_SAMPLE_TIMES=50;                    //define how many samples you are going to take in the calibration phase
int CALIBRATION_SAMPLE_INTERVAL=500;                //define the time interal(in milisecond) between each samples in the
                                                    //cablibration phase
int READ_SAMPLE_INTERVAL=50;                        //define how many samples you are going to take in normal operation
int READ_SAMPLE_TIMES=5;                            //define the time interal(in milisecond) between each samples in
                                                    //normal operation

/**********************Application Related Macros**********************************/
#define  GAS_CO  1

/*****************************Globals***********************************************/

float COCurve[3]  =  {1,0.462,-0.23};    //two points are taken from the curve.
//data format:{ x, y, slope}; point1: (lg200, 0.53), point2: (lg10000,  -0.22)
float Ro =  10;                 //Ro is initialized to 10 kilo ohms
int status = WL_IDLE_STATUS;

void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Conectado a:");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  randomSeed(micros());

  Serial.println("");
  Serial.println("WiFi conectado");
  Serial.println("IP Dirección: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  // Switch on the LED if an 1 was received as first character
  if ((char)payload[0] == '1') {
    digitalWrite(BUILTIN_LED, LOW);   // Turn the LED on (Note that LOW is the voltage level
    // but actually the LED is on; this is because
    // it is active low on the ESP-01)
    enviarDatos = true;
  } else {
    digitalWrite(BUILTIN_LED, HIGH);  // Turn the LED off by making the voltage HIGH
    enviarDatos = false;
  }


}

void reconnect()
{
  // Loop until we're reconnected
  while (!client.connected())
  {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (client.connect(clientId.c_str(), mqttUser, mqttPassword ))
    {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("aire/componente", id);
      // ... and resubscribe
      client.subscribe("componente");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" Intentando en 5 seguntos");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void setup()
{
  pinMode(BUILTIN_LED, OUTPUT);     // Initialize the BUILTIN_LED pin as an output
  Serial.begin(115200);
  setup_wifi();

  // --- configuracion sensor
  pinMode(calibrationLed,OUTPUT);
  digitalWrite(calibrationLed,HIGH);
  Serial.println("....");
  Serial.print("Calibrando...");
  Ro = MQCalibration(MQ_PIN);                         //Calibrating the sensor. Please make sure the sensor is in clean air
  digitalWrite(calibrationLed,LOW);

  Serial.println("hecho!");
  Serial.print("Ro= ");
  Serial.print(Ro);
  Serial.println("kohm\n");
  delay(2000);
  // termina  --- configuracion sensor

  client.setServer(mqttServer, mqttPort);
  client.setCallback(callback);
}

void loop() {

  if (!client.connected()) {
    reconnect();
  }
  client.loop();

  Serial.print("Se esta conectado al servidor. EJución loop.. ");

    // Hacemos pausa de dos segundos antes de cada nueva medición
    //al sensor le cuesta 250ms leer estos datos
   delay(2000);
  long iPPM_CO = 0;

  iPPM_CO = MQGetGasPercentage(MQRead(MQ_PIN)/Ro,GAS_CO);
  Serial.println("**************************** CONCENTRACIÓN DE GASES ****************************");

   Serial.print("CO: ");
   Serial.print(iPPM_CO);
   Serial.println(" ppm");


   Serial.println("*******************************************************************************\n");
   Serial.println();

  delay(500);

  Serial.println("Enviar datos?");
  unsigned long now = millis();
  if (now - lastMsg > 2000 )
  {
    lastMsg = now;

    datos["medicion"] = iPPM_CO;
    datos["lat"] = lat;
    datos["lng"] = lng;
    datos["id"] = id;

    char jsonChar[100];
    datos.printTo((char*)jsonChar, datos.measureLength() + 1);
    client.publish("aire", jsonChar);
    Serial.println("Datos enviados al servidor");
    Serial.println(jsonChar);
  }

  // Añadimos un retraso para limtitar el número de escrituras en Thinhspeak
  int duracionDelay = 20; //En segundos
  for (int i = 0; i < duracionDelay; i ++) { //Esto es debido a que el máximo que el Arduino puede procesar con precisión es 5000ms o 5 segundos
    delay (1000);
  }

}


/****************** MQResistanceCalculation ****************************************
Input:   raw_adc - raw value read from adc, which represents the voltage
Output:  the calculated sensor resistance
Remarks: The sensor and the load resistor forms a voltage divider. Given the voltage
        across the load resistor and its resistance, the resistance of the sensor
        could be derived.
************************************************************************************/
float MQResistanceCalculation(int raw_adc)
{
 return ( ((float)RL_VALUE*(1023-raw_adc)/raw_adc));
}

/***************************** MQCalibration ****************************************
Input:   mq_pin - analog channel
Output:  Ro of the sensor
Remarks: This function assumes that the sensor is in clean air. It use
        MQResistanceCalculation to calculates the sensor resistance in clean air
        and then divides it with RO_CLEAN_AIR_FACTOR. RO_CLEAN_AIR_FACTOR is about
        10, which differs slightly between different sensors.
************************************************************************************/
float MQCalibration(int mq_pin)
{
 int i;
 float val=0;

 for (i=0;i<CALIBARAION_SAMPLE_TIMES;i++) {            //take multiple samples
   val += MQResistanceCalculation(analogRead(mq_pin));
   delay(CALIBRATION_SAMPLE_INTERVAL);
 }
 val = val/CALIBARAION_SAMPLE_TIMES;                   //calculate the average value
 val = val/RO_CLEAN_AIR_FACTOR;                        //divided by RO_CLEAN_AIR_FACTOR yields the Ro
 return val;                                                      //according to the chart in the datasheet

}

/*****************************  MQRead *********************************************
Input:   mq_pin - analog channel
Output:  Rs of the sensor
Remarks: This function use MQResistanceCalculation to caculate the sensor resistenc (Rs).
        The Rs changes as the sensor is in the different consentration of the target
        gas. The sample times and the time interval between samples could be configured
        by changing the definition of the macros.
************************************************************************************/
float MQRead(int mq_pin)
{
 int i;
 float rs=0;

 for (i=0;i<READ_SAMPLE_TIMES;i++) {
   rs += MQResistanceCalculation(analogRead(mq_pin));
   delay(READ_SAMPLE_INTERVAL);
 }

 rs = rs/READ_SAMPLE_TIMES;

 return rs;
}

/*****************************  MQGetGasPercentage **********************************
Input:   rs_ro_ratio - Rs divided by Ro
        gas_id      - target gas type
Output:  ppm of the target gas
Remarks: This function passes different curves to the MQGetPercentage function which
        calculates the ppm (parts per million) of the target gas.
************************************************************************************/


long MQGetGasPercentage(float rs_ro_ratio, int gas_id)
{

 if ( gas_id == GAS_CO ) {
    return MQGetPercentage(rs_ro_ratio,COCurve);
 }


 return 0;
}

/*****************************  MQGetPercentage **********************************
Input:   rs_ro_ratio - Rs divided by Ro
        pcurve      - pointer to the curve of the target gas
Output:  ppm of the target gas
Remarks: By using the slope and a point of the line. The x(logarithmic value of ppm)
        of the line could be derived if y(rs_ro_ratio) is provided. As it is a
        logarithmic coordinate, power of 10 is used to convert the result to non-logarithmic
        value.
************************************************************************************/
long  MQGetPercentage(float rs_ro_ratio, float *pcurve)
{
 return (pow(10,( ((log(rs_ro_ratio)-pcurve[1])/pcurve[2]) + pcurve[0])));
}
